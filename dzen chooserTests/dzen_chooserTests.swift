//
//  dzen_chooserTests.swift
//  dzen chooserTests
//
//  Created by r.khomenko on 11.12.2022.
//

import XCTest
@testable import dzen_chooser

final class dzen_chooserTests: XCTestCase {
	func testInitialization() throws {
		let viewModel = TopicsViewModel(topics: [
			Topic(description: "1"),
			Topic(description: "2"),
		])
		XCTAssertEqual(
			viewModel.topics,
			[
				Topic(description: "1"),
				Topic(description: "2"),
			]
		)
	}
	
	func testDidTapCell() throws {
		let viewModel = TopicsViewModel(topics: [
			Topic(description: "1"),
			Topic(description: "2"),
		])
		var didTappedTopic = Topic(description: "1")
		didTappedTopic.isSelected = true
		
		viewModel.handleViewAction(action: .didTapCell(0))
		XCTAssertEqual(
			didTappedTopic,
			viewModel.topics[0]
		)
	}
	
	func testCancelAllSelection() throws {
		var selectedTopic: Topic {
			var topic = Topic(description: "topic")
			topic.isSelected = true
			return topic
		}
		var unselectedTopic: Topic {
			var topic = Topic(description: "topic")
			topic.isSelected = false
			return topic
		}
		
		var viewModel = TopicsViewModel(topics: [
			selectedTopic,
			selectedTopic,
		])
		
		viewModel.handleViewAction(action: .cancelAllSelection)
		XCTAssertEqual(
			viewModel.topics,
			[unselectedTopic, unselectedTopic]
		)
	}
	
	func testAddToFavorites() throws {
		var selectedTopic: Topic {
			var topic = Topic(description: "topic")
			topic.isSelected = true
			return topic
		}
		
		var viewModel = TopicsViewModel(topics: [
			selectedTopic,
			selectedTopic,
		])
		
		viewModel.handleViewAction(action: .addToFavorites)
		XCTAssertEqual(viewModel.topics, [])
	}
	
	func testCompletion() throws {
		var selectedTopic: Topic {
			var topic = Topic(description: "topic")
			topic.isSelected = true
			return topic
		}
		var viewModel = TopicsViewModel(topics: [
			selectedTopic,
			selectedTopic,
		])
		
		viewModel.completion = { topics in
			XCTAssertEqual(
				topics,
				viewModel.topics
			)
		}
		viewModel.completion?(viewModel.topics)
	}
	
	func testHeaderTitle() throws {
		var selectedTopic: Topic {
			var topic = Topic(description: "topic")
			topic.isSelected = true
			return topic
		}
		var unselectedTopic: Topic {
			var topic = Topic(description: "topic")
			topic.isSelected = false
			return topic
		}
		var viewModel = TopicsViewModel(topics: [selectedTopic, selectedTopic])
		XCTAssertEqual(viewModel.setHeaderTitle(), GlobalConstants.placeholderConfirmTitle)
		
		viewModel = TopicsViewModel(topics: [unselectedTopic, unselectedTopic])
		XCTAssertEqual(viewModel.setHeaderTitle(), GlobalConstants.placeholderTitle)
	}
}
