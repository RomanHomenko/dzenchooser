//
//  GlobalConstants.swift
//  dzen chooser
//
//  Created by r.khomenko on 11.12.2022.
//

import Foundation

public struct GlobalConstants {
	static let offsetConstant: CGFloat = 16
	static let placeholderTopics: [Topic] = [
		Topic(description: "Юмор"),
		Topic(description: "Еда"),
		Topic(description: "Кино"),
		Topic(description: "Рестораны"),
		Topic(description: "Прогулки"),
		Topic(description: "Политика"),
		Topic(description: "Новости"),
		Topic(description: "Автомобили"),
		Topic(description: "Сериалы"),
		Topic(description: "Рецепты"),
		Topic(description: "Работа"),
		Topic(description: "Отдых"),
		Topic(description: "Спорт"),
		Topic(description: "Политика"),
		Topic(description: "Новости"),
	]
	static let placeholderTitle: String = "Отметьте то, что вам интересно, чтобы настроить Дзен"
	static let placeholderConfirmTitle: String = "Открыть подходящие новости?"
}
