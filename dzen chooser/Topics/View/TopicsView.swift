//
//  TopicsViewController.swift
//  dzen chooser
//
//  Created by r.khomenko on 11.12.2022.
//

import UIKit
import Foundation

final class TopicsViewController: UIViewController {
	private struct Constants {
		static let headerHeight: CGFloat = 64
		static let footerHeight: CGFloat = 60
	}
	
	public var topicsViewModel: TopicsViewModel! {
		didSet {
			topicsViewModel.stateBlock = { [weak self] state in
				self?.handleState(state: state)
			}
		}
	}
	
	private let descriptionLabel = UILabel()
	private var collectionView = UICollectionView(
		frame: .zero,
		collectionViewLayout: .topicsLeftLayout
	)

	override func viewDidLoad() {
		super.viewDidLoad()
		
		setupViews()
		setupConstraints()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		topicsViewModel.handleViewAction(action: .initial)
	}
	
	private func setupViews() {
		view.backgroundColor = .tBlack
		collectionView.register(
			DzenCollectionViewCell.self,
			forCellWithReuseIdentifier: DzenCollectionViewCell.id
		)
		collectionView.register(
			HeaderTopicsCollectionReusableView.self,
			forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
			withReuseIdentifier: HeaderTopicsCollectionReusableView.id
		)
		collectionView.register(
			FooterTopicsCollectionReusableView.self,
			forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter,
			withReuseIdentifier: FooterTopicsCollectionReusableView.id
		)
		collectionView.delegate = self
		collectionView.dataSource = self
		collectionView.backgroundColor = .tBlack
		view.addSubview(collectionView)
	}
	
	private func setupConstraints() {
		collectionView.translatesAutoresizingMaskIntoConstraints = false
		NSLayoutConstraint.activate([
			collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
			collectionView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: GlobalConstants.offsetConstant),
			collectionView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: -GlobalConstants.offsetConstant),
			collectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
		])
	}
}

extension TopicsViewController {
	private func handleState(state: TopicsViewModel.State) {
		switch state {
		case .didUpdated:
			collectionView.reloadData()
		}
	}
}

// MARK: - Delegate
extension TopicsViewController: UICollectionViewDelegate {
	public func collectionView(
		_ collectionView: UICollectionView,
		numberOfItemsInSection section: Int
	) -> Int {
		return topicsViewModel.topics.count
	}
	
	public func collectionView(
		_ collectionView: UICollectionView,
		didHighlightItemAt indexPath: IndexPath
	) {
		UIView.animate(withDuration: 0.1) {
			if let cell = collectionView.cellForItem(
				at: indexPath
			) as? DzenCollectionViewCell {
				cell.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)
			}
		}
	}
	
	public func collectionView(
		_ collectionView: UICollectionView,
		didUnhighlightItemAt indexPath: IndexPath
	) {
		UIView.animate(withDuration: 0.5, delay: 1) {
			if let cell = collectionView.cellForItem(
				at: indexPath
			) as? DzenCollectionViewCell {
				cell.transform = .identity
				self.topicsViewModel.handleViewAction(
					action: .didTapCell(indexPath.row)
				)
			}
		}
	}
}

// MARK: - DataSource
extension TopicsViewController: UICollectionViewDataSource {
	public func collectionView(
		_ collectionView: UICollectionView,
		cellForItemAt indexPath: IndexPath
	) -> UICollectionViewCell {
		guard let cell = collectionView.dequeueReusableCell(
			withReuseIdentifier: DzenCollectionViewCell.id,
			for: indexPath
		) as? DzenCollectionViewCell else {
			return UICollectionViewCell()
		}
		let topic = self.topicsViewModel.topics[indexPath.row]
		cell.setup(with: topic)
		
		return cell
	}
	
	public func collectionView(
		_ collectionView: UICollectionView,
		viewForSupplementaryElementOfKind kind: String,
		at indexPath: IndexPath
	) -> UICollectionReusableView {
		switch kind {
		case UICollectionView.elementKindSectionHeader:
			if let header = collectionView.dequeueReusableSupplementaryView(
				ofKind: UICollectionView.elementKindSectionHeader,
				withReuseIdentifier: HeaderTopicsCollectionReusableView.id,
				for: indexPath
			) as? HeaderTopicsCollectionReusableView {
				header.configure(
					title: topicsViewModel.setHeaderTitle(),
					buttonAction: { [weak self] in
						self?.topicsViewModel.handleViewAction(action: .cancelAllSelection)
					}
				)
				return header
			}
			return HeaderTopicsCollectionReusableView()
		case UICollectionView.elementKindSectionFooter:
			if let footer = collectionView.dequeueReusableSupplementaryView(
				ofKind: UICollectionView.elementKindSectionFooter,
				withReuseIdentifier: FooterTopicsCollectionReusableView.id,
				for: indexPath
			) as? FooterTopicsCollectionReusableView {
				footer.configure { [weak self] in
					self?.topicsViewModel.handleViewAction(action: .addToFavorites)
				}
				return footer
			}
			return FooterTopicsCollectionReusableView()
		default:
			assert(false, "Unexpected element kind")
		}
	}
}

// MARK: - FlowLayout
extension TopicsViewController: UICollectionViewDelegateFlowLayout {
	public func collectionView(
		_ collectionView: UICollectionView,
		layout collectionViewLayout: UICollectionViewLayout,
		referenceSizeForHeaderInSection section: Int
	) -> CGSize {
		return headerFooterSize(with: Constants.headerHeight)
	}
	
	public func collectionView(
		_ collectionView: UICollectionView,
		layout collectionViewLayout: UICollectionViewLayout,
		referenceSizeForFooterInSection section: Int
	) -> CGSize {
		for topic in topicsViewModel.topics {
			if topic.isSelected {
				return headerFooterSize(with: Constants.footerHeight)
			}
		}
		return .zero
	}
}
