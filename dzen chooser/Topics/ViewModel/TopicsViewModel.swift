//
//  TopicsViewModel.swift
//  dzen chooser
//
//  Created by r.khomenko on 11.12.2022.
//

import Foundation

final class TopicsViewModel {
	private(set) var topics: [Topic]
	private var favoritesTopics: [Topic] = []
	
	public var stateBlock: ((State) -> ())?
	public var completion: (([Topic]) -> ())?
	
	public init(topics: [Topic]) {
		self.topics = topics
	}
	
	public func handleViewAction(action: ViewAction) {
		switch action {
		case .initial:
			getTopics()
		case .didTapCell(let index):
			changeTopicState(at: index)
		case .cancelAllSelection:
			bringBackSelected()
		case .addToFavorites:
			saveFavorites()
		}
	}
	
	public func setHeaderTitle() -> String {
		for topic in topics {
			if topic.isSelected {
				return GlobalConstants.placeholderConfirmTitle
			}
		}
		return GlobalConstants.placeholderTitle
	}
	
	private func getTopics() {
		self.stateBlock?(.didUpdated)
	}
	
	private func bringBackSelected() {
		for index in 0..<favoritesTopics.count {
			var favTopic = favoritesTopics[index]
			favTopic.isSelected = false
			topics.append(favTopic)
		}
		favoritesTopics.removeAll()
		self.stateBlock?(.didUpdated)
	}
	
	private func changeTopicState(at index: Int) {
		topics[index].isSelected.toggle()
		if topics[index].isSelected {
			favoritesTopics.append(topics[index])
		} else {
			let newFavorites = favoritesTopics.filter { $0 != topics[index] }
			favoritesTopics = newFavorites
		}
		self.stateBlock?(.didUpdated)
	}
	
	private func saveFavorites() {
		let newTopics = topics.filter { $0.isSelected != true }
		topics = newTopics
		self.stateBlock?(.didUpdated)
	}
}

extension TopicsViewModel {
	enum ViewAction {
		case initial
		case didTapCell(Int)
		case cancelAllSelection
		case addToFavorites
	}
	
	enum State {
		case didUpdated
	}
}
