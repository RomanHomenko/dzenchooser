//
//  Topic.swift
//  dzen chooser
//
//  Created by r.khomenko on 11.12.2022.
//

import Foundation

public struct Topic: Equatable {
	var description: String = ""
	var isSelected: Bool = false
	
	init(description: String) {
		self.description = description
	}
	
	public static func == (lhs: Topic, rhs: Topic) -> Bool {
		guard lhs.isSelected == lhs.isSelected else { return false }
		guard lhs.description == lhs.description else { return false }
		return true
	}
}
