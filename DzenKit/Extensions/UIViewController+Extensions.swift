//
//  UIViewController+Extensions.swift
//  dzen chooser
//
//  Created by r.khomenko on 11.12.2022.
//

import UIKit

public extension UIViewController {
	func headerFooterSize(with hight: CGFloat) -> CGSize {
		return CGSize(
			width: view.frame.width - (GlobalConstants.offsetConstant * 2),
			height: hight
		)
	}
}
