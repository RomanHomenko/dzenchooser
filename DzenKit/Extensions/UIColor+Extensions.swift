//
//  UIColor+Extensions.swift
//  dzen chooser
//
//  Created by r.khomenko on 11.12.2022.
//

import UIKit

public extension UIColor {
	static let tWhite = UIColor(
		red: 1.0,
		green: 1.0,
		blue: 1.0,
		alpha: 1.0
	)
	
	static let tGray1 = UIColor(
		red: 1.0,
		green: 1.0,
		blue: 1.0,
		alpha: 0.12
	)
	
	static let tGray2 = UIColor(
		red: 1.0,
		green: 1.0,
		blue: 1.0,
		alpha: 0.17
	)
	
	static let tGray3 = UIColor(
		red: 1.0,
		green: 1.0,
		blue: 1.0,
		alpha: 0.27
	)
	
	static let tGray4 = UIColor(
		red: 0.96,
		green: 0.96,
		blue: 0.96,
		alpha: 1.0
	)
	
	static let tBlack = UIColor(
		red: 0.0,
		green: 0.0,
		blue: 0.0,
		alpha: 1
	)
	
	static let tHeaderText = UIColor(
		red: 1.0,
		green: 1.0,
		blue: 1.0,
		alpha: 0.48
	)
	
	static let tOrange = UIColor(
		red: 1.0,
		green: 0.33,
		blue: 0.09,
		alpha: 1.0
	)
}
