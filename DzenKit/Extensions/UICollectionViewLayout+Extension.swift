//
//  UICollectionViewLayout+Extension.swift
//  dzen chooser
//
//  Created by r.khomenko on 11.12.2022.
//

import UIKit

public extension UICollectionViewLayout {
	static var topicsLeftLayout: UICollectionViewFlowLayout {
		let layout = TopicsCollectionViewFlowLayout(
			minimumInteritemSpacing: 8,
			minimumLineSpacing: 8
		)
		layout.scrollDirection = .vertical
		return layout
	}
}
