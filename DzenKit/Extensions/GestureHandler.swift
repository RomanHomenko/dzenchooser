//
//  DzenButton.swift
//  dzen chooser
//
//  Created by r.khomenko on 12.12.2022.
//

import Foundation
import UIKit

public class GestureHandler: UIView {
	open var isHighlighted: Bool = false
	
	public var action: (() -> Void)? = nil
	
	public override func touchesBegan(
		_ touches: Set<UITouch>,
		with event: UIEvent?
	) {
		if event?.type == .touches {
			isHighlighted = true
		}
		super.touchesBegan(touches, with: event)
	}

	public override func touchesCancelled(
		_ touches: Set<UITouch>,
		with event: UIEvent?
	) {
		if event?.type == .touches {
			isHighlighted = true
		}
		action?()
		super.touchesCancelled(touches, with: event)
	}
	
	public override func touchesEnded(
		_ touches: Set<UITouch>,
		with event: UIEvent?
	) {
		if event?.type == .touches {
			isHighlighted = false
		}
		action?()
		super.touchesEnded(touches, with: event)
	}
	
	public func addAction(_ action: @escaping () -> Void) {
		self.action = action
	}
}
