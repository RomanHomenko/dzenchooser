//
//  DzenButton.swift
//  dzen chooser
//
//  Created by r.khomenko on 12.12.2022.
//

import Foundation
import UIKit

final public class DzenButton: GestureHandler {
	public override var isHighlighted: Bool {
		didSet {
			let highlighted = style.backgroundColor.withAlphaComponent(
				style.alphaComponent
			)
			let normal = style.backgroundColor
			backgroundColor = isHighlighted ? highlighted : normal
		}
	}
	
	private var style: Style = Style.default
	private var label: UILabel = UILabel()
	
	public init(
		title: String = "",
		style: DzenButton.Style = .default,
		action: @escaping (() -> Void) = {}
	) {
		super.init(frame: .zero)
		
		label.text = title
		self.action = action
		setupViews()
		setupConstraints()
		applyStyle(style)
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	public func setTitle(_ title: String) {
		label.text = title
	}
	
	public override func addAction(_ action: @escaping () -> Void) {
		self.action = action
	}
	
	private func setupViews() {
		addSubview(label)
	}
	
	private func setupConstraints() {
		label.translatesAutoresizingMaskIntoConstraints = false
		NSLayoutConstraint.activate([
			label.topAnchor.constraint(equalTo: self.topAnchor),
			label.leftAnchor.constraint(equalTo: self.leftAnchor),
			label.rightAnchor.constraint(equalTo: self.rightAnchor),
			label.bottomAnchor.constraint(equalTo: self.bottomAnchor),
		])
	}
}

public extension DzenButton {
	struct Style {
		public let font: UIFont
		public let textColor: UIColor
		public let backgroundColor: UIColor
		public let cornerRadius: CGFloat
		public let alphaComponent: CGFloat
		
		public init(
			font: UIFont,
			textColor: UIColor,
			backgroundColor: UIColor,
			cornerRadius: CGFloat,
			alphaComponent: CGFloat
		) {
			self.font = font
			self.textColor = textColor
			self.backgroundColor = backgroundColor
			self.cornerRadius = cornerRadius
			self.alphaComponent = alphaComponent
		}
		
		public static let `default` = Style(
			font: UIFont.systemFont(ofSize: 16),
			textColor: .black,
			backgroundColor: UIColor.clear,
			cornerRadius: .zero,
			alphaComponent: 1.0
		)
		
		public static let bigWhite = Style(
			font: UIFont.systemFont(
				ofSize: 16,
				weight: .medium
			),
			textColor: .tBlack,
			backgroundColor: .tWhite,
			cornerRadius: 20,
			alphaComponent: 0.8
		)
		
		public static let smallGray = Style(
			font: UIFont.systemFont(
				ofSize: 16,
				weight: .medium
			),
			textColor: .tWhite,
			backgroundColor: .tGray1,
			cornerRadius: 20,
			alphaComponent: 0.096
		)
	}
	
	func applyStyle(_ style: Style) {
		self.style = style
		label.font = style.font
		label.textColor = style.textColor
		label.textAlignment = .center
		backgroundColor = style.backgroundColor
		layer.cornerRadius = style.cornerRadius
	}
}
