//
//  TopicsCollectionViewFlowLayout.swift
//  dzen chooser
//
//  Created by r.khomenko on 11.12.2022.
//

import UIKit

public final class TopicsCollectionViewFlowLayout: UICollectionViewFlowLayout {
	required init(
		minimumInteritemSpacing: CGFloat = 0,
		minimumLineSpacing: CGFloat = 0,
		sectionInset: UIEdgeInsets = UIEdgeInsets(
			top: 16,
			left: 0,
			bottom: 16,
			right: 0
		)
	) {
		super.init()
		
		estimatedItemSize = UICollectionViewFlowLayout.automaticSize
		self.minimumInteritemSpacing = minimumInteritemSpacing
		self.minimumLineSpacing = minimumLineSpacing
		self.sectionInset = sectionInset
		self.sectionInsetReference = .fromSafeArea
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
	override public func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
		var newAttributesArray = [UICollectionViewLayoutAttributes]()
		guard let superAttributesArray = super.layoutAttributesForElements(in: rect) else {
			return []
		}
		for (index, attributes) in superAttributesArray.enumerated() {
			if index == 0 || superAttributesArray[index - 1].frame.origin.y != attributes.frame.origin.y {
				attributes.frame.origin.x = sectionInset.left
			} else {
				let previousAttributes = superAttributesArray[index - 1]
				let previousFrameRight = previousAttributes.frame.origin.x + previousAttributes.frame.width
				attributes.frame.origin.x = previousFrameRight + minimumInteritemSpacing
			}
			newAttributesArray.append(attributes)
		}
		return newAttributesArray
	}
}
