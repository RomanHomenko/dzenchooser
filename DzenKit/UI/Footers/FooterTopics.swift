//
//  FooterTopics.swift
//  dzen chooser
//
//  Created by r.khomenko on 11.12.2022.
//

import UIKit

final class FooterTopicsCollectionReusableView: UICollectionReusableView {
	static let id = "topicFooter"
	
	private var button = DzenButton()
	
	public func configure(
		buttonTitle: String = "Продолжить",
		buttonAction: @escaping () -> Void
	) {
		setupViews()
		button.addAction(buttonAction)
		button.setTitle(buttonTitle)
	}
	
	override func layoutSubviews() {
		setupConstraints()
	}
	
	private func setupViews() {
		backgroundColor = .tBlack
		button.applyStyle(.bigWhite)
		
		addSubview(button)
	}
	
	private func setupConstraints() {
		button.translatesAutoresizingMaskIntoConstraints = false
		NSLayoutConstraint.activate([
			button.topAnchor.constraint(equalTo: self.topAnchor, constant: 20),
			button.bottomAnchor.constraint(equalTo: self.bottomAnchor),
			button.leftAnchor.constraint(equalTo: self.leftAnchor),
			button.rightAnchor.constraint(equalTo: self.rightAnchor),
		])
	}
}
