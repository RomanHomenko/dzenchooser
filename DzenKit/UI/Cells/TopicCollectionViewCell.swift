//
//  DzenCollectionViewCell.swift
//  dzen chooser
//
//  Created by r.khomenko on 11.12.2022.
//

import UIKit

final class DzenCollectionViewCell: UICollectionViewCell {
	private struct Constants {
		static let cornerRadius: CGFloat = 12
		static let fontSize: CGFloat = 16
	}
	
    public static let id = "topicCell"
	
	private let title = UILabel()
	private let container = UIView()
	private let separator = UIView()
	private let stateImage = UIImageView()
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		setupViews()
		setupConstraints()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	public func setup(with topic: Topic) {
		title.text = topic.description
		updateSelectedCell(topic.isSelected)
	}
	
	private func setupViews() {
		backgroundColor = .tGray2
		layer.cornerRadius = Constants.cornerRadius
		title.font = .systemFont(
			ofSize: Constants.fontSize,
			weight: .medium
		)
		title.textColor = .tWhite
		title.textAlignment = .center
		separator.backgroundColor = .tGray3
		let image = UIImage(
			named: "plus_white",
			in: .main,
			with: nil
		)
		stateImage.image = image
		stateImage.contentMode = .scaleToFill
		stateImage.tintColor = .white
		addSubview(title)
		addSubview(container)
	}
	
	private func setupConstraints() {
		title.translatesAutoresizingMaskIntoConstraints = false
		NSLayoutConstraint.activate([
			title.topAnchor.constraint(equalTo: self.topAnchor, constant: 8),
			title.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -8),
			title.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 12),
			title.rightAnchor.constraint(equalTo: container.leftAnchor, constant: -6),
		])
		
		container.translatesAutoresizingMaskIntoConstraints = false
		container.addSubview(separator)
		container.addSubview(stateImage)
		
		NSLayoutConstraint.activate([
			container.topAnchor.constraint(equalTo: self.topAnchor, constant: 8),
			container.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -8),
			container.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8),
		])
		
		separator.translatesAutoresizingMaskIntoConstraints = false
		NSLayoutConstraint.activate([
			separator.widthAnchor.constraint(equalToConstant: 1),
			separator.heightAnchor.constraint(equalToConstant: 20),
			separator.centerYAnchor.constraint(equalTo: container.centerYAnchor),
			separator.leftAnchor.constraint(equalTo: container.leftAnchor, constant: 8),
		])
		
		stateImage.translatesAutoresizingMaskIntoConstraints = false
		NSLayoutConstraint.activate([
			stateImage.leftAnchor.constraint(equalTo: separator.rightAnchor, constant: 11),
			stateImage.rightAnchor.constraint(equalTo: container.rightAnchor, constant: -3),
			stateImage.centerYAnchor.constraint(equalTo: container.centerYAnchor),
		])
	}
	
	private func updateSelectedCell(_ isSelected: Bool) {
		if isSelected {
			backgroundColor = .tOrange
			separator.isHidden = isSelected
			let image = UIImage(named: "checkMark_white")
			stateImage.image = image
		} else {
			backgroundColor = .tGray2
			separator.isHidden = isSelected
			let image = UIImage(named: "plus_white")
			stateImage.image = image
		}
	}
}
