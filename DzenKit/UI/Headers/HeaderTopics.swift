//
//  HeaderTopics.swift
//  dzen chooser
//
//  Created by r.khomenko on 11.12.2022.
//

import UIKit

final class HeaderTopicsCollectionReusableView: UICollectionReusableView {
	private struct Constants {
		static let headerButtonWidth: CGFloat = 80
		static let fontSize: CGFloat = 16
	}
	
	public static let id = "topicHeader"
	
	private var title = UILabel()
	private var button = DzenButton()
	
	override func layoutSubviews() {
		setupConstraints()
	}
	
	public func configure(
		title: String = "Placeholder",
		buttonTitle: String = "Позже",
		buttonAction: @escaping () -> Void
	) {
		self.title.text = title
		button.addAction(buttonAction)
		button.setTitle(buttonTitle)
		setupViews()
	}
	
	private func setupViews() {
		backgroundColor = .tBlack
		
		title.textColor = .tHeaderText
		title.textAlignment = .left
		title.font = .systemFont(
			ofSize: Constants.fontSize,
			weight: .medium
		)
		title.numberOfLines = 0
		addSubview(title)
		
		button.applyStyle(.smallGray)
		addSubview(button)
	}
	
	private func setupConstraints() {
		title.translatesAutoresizingMaskIntoConstraints = false
		NSLayoutConstraint.activate([
			title.topAnchor.constraint(equalTo: self.topAnchor, constant: GlobalConstants.offsetConstant),
			title.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -8),
			title.leftAnchor.constraint(equalTo: self.leftAnchor),
			title.rightAnchor.constraint(equalTo: button.leftAnchor),
		])
		
		button.translatesAutoresizingMaskIntoConstraints = false
		NSLayoutConstraint.activate([
			button.topAnchor.constraint(equalTo: self.topAnchor, constant: GlobalConstants.offsetConstant),
			button.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -8),
			button.rightAnchor.constraint(equalTo: self.rightAnchor),
			button.widthAnchor.constraint(equalToConstant: Constants.headerButtonWidth),
		])
	}
}
