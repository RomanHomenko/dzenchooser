# VK Service app

## Description

* App was created for VK Cup 2022
* Track: Mobile
* We can choose the topics we like and watched selected from buffer

## Screenshots
<img src="Screenshots/allDisabled.png" width="400" height="790">
<img src="Screenshots/someSelected.png" width="400" height="790">

## Techonologies

* UI Components - `UIKit`
* Architecture - `Model-View-ViewModel`
* 100% Programmatical UI
* 100% Programmatical `AutoLayout` 
* Was used `UICollectionView`
* Was used `UICollectionViewFlowLayout`
* Was created custom `UITableViewCell` 
* User can tap on cell with `UIAnimation` and change it state
* Button is hidden while all cell are disabled
* Test app with `XCTest`
* UnitTests were added for `TopicsViewModel`
* UITests were added for `TopicsViewController`

## Getting Started

### Dependencies

* IPhone or IPad  
* IOS 13

### Installing

* Download from GitLab

### Executing program

* Run it with `XCode`

### How to use it

* Tap on favorites topics
* Tap "Продолжить" to hide favorites topics from collectionView
* Tap "Позже" to clear all favorites topics and comeback all default topics   

## Authors

* Roman Khomenko  
* Telegram - [@romanKhomenko]
* VK - (https://vk.com/tolstiykroll)

## Version History

* 1.0
    * Initial Release

