//
//  dzen_chooserUITests.swift
//  dzen chooserUITests
//
//  Created by r.khomenko on 11.12.2022.
//

import XCTest

final class dzen_chooserUITests: XCTestCase {
	let app = XCUIApplication()

	override func setUpWithError() throws {
		continueAfterFailure = false
		app.launch()
	}
	
	override func tearDownWithError() throws {
		continueAfterFailure = true
		app.terminate()
	}
	
	func testCellsExist() {
		let collectionViewsQuery = XCUIApplication().collectionViews
		
		XCTAssertTrue(collectionViewsQuery.cells.staticTexts["Юмор"].exists)
		XCTAssertTrue(collectionViewsQuery.cells.staticTexts["Еда"].exists)
	}
	
	func testCellsAddedToFavorites() {
		let collectionViewsQuery = XCUIApplication().collectionViews
		
		collectionViewsQuery.cells.staticTexts["Юмор"].tap()
		collectionViewsQuery.cells.staticTexts["Еда"].tap()

		XCTAssertTrue(collectionViewsQuery.staticTexts["Продолжить"].exists)
		collectionViewsQuery.staticTexts["Продолжить"].tap()

		XCTAssertFalse(collectionViewsQuery.cells.staticTexts["Юмор"].exists)
		XCTAssertFalse(collectionViewsQuery.cells.staticTexts["Еда"].exists)
	}
	
	func testBringBackFromFavorites() {
		let collectionViewsQuery = XCUIApplication().collectionViews
		
		collectionViewsQuery.cells.staticTexts["Юмор"].tap()
		collectionViewsQuery.cells.staticTexts["Еда"].tap()

		collectionViewsQuery.staticTexts["Продолжить"].tap()

		XCTAssertFalse(collectionViewsQuery.cells.staticTexts["Юмор"].exists)
		XCTAssertFalse(collectionViewsQuery.cells.staticTexts["Еда"].exists)
		
		collectionViewsQuery.staticTexts["Позже"].tap()

		XCTAssertTrue(collectionViewsQuery.cells.staticTexts["Юмор"].exists)
		XCTAssertTrue(collectionViewsQuery.cells.staticTexts["Еда"].exists)
	}
}
